# GNOME Brand Design

This repository contains work in progress design work for the GNOME brand style. Live at [brand.gnome.org](https://brand.gnome.org)


# License
[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](http://creativecommons.org/licenses/by-sa/4.0/)
